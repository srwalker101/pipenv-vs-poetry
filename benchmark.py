#!/usr/bin/env python

import subprocess as sp
import time
import os
from pathlib import Path
import logging

logger = logging.getLogger("benchmark")
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

fh = logging.FileHandler("log.txt", "w")
fh.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s - %(message)s")
ch.setFormatter(formatter)
fh.setFormatter(formatter)

logger.addHandler(ch)
logger.addHandler(fh)


class TestBase:
    def benchmark(self):
        logger.info(self.NAME)
        self.fresh_install_no_venv()
        self.fresh_install_with_venv()
        self.adding_package()
        self.installing_from_lockfile()
        self.generating_requirements()

    def _collect_durations(self, text, fn):
        durations = []
        logger.info(f"starting {text}")
        for _ in range(self.n_tests):
            duration = fn()
            logger.debug(f"test duration: {duration} s")
            durations.append(duration)

        logger.info(f"{text} complete: {min(durations)}")

    def fresh_install_with_venv(self):
        self._collect_durations(
            "fresh install with venv", self._fresh_install_with_venv
        )

    def fresh_install_no_venv(self):
        self._collect_durations("fresh install no venv", self._fresh_install_no_venv)

    def adding_package(self):
        self._collect_durations("adding package", self._adding_package)

    def installing_from_lockfile(self):
        self._collect_durations(
            "installing from lockfile", self._installing_from_lockfile
        )

    def generating_requirements(self):
        self._collect_durations(
            "generating requirements", self._generating_requirements
        )

    def _run(self, *args, **kwargs):
        kwargs["cwd"] = self.cwd
        return sp.run(*args, **kwargs)


class Pipenv(TestBase):

    DEFAULT = """
[[source]]
url = "https://pypi.org/simple"
verify_ssl = true
name = "pypi"

[packages]
boto3 = "*"
pyjwt = "*"
requests = "*"
flask = "*"
cryptography = "*"

[dev-packages]
pytest = "*"
pytest-mock = "*"
moto = "*"
pytest-randomly = "*"

[requires]
python_version = "3.8"
    """
    NAME = "pipenv"

    def __init__(self, n_tests):
        self.n_tests = n_tests
        self.cwd = "pipenv"

    def _reset_pipfile(self):
        with open("pipenv/Pipfile", "w") as outfile:
            outfile.write(self.DEFAULT)

    def _fresh_install_no_venv(self):
        self._remove_venv()
        try:
            os.remove(f"{self.cwd}/Pipfile.lock")
        except FileNotFoundError:
            pass
        tstart = time.time()
        self._run(["pipenv", "install"], check=True)
        tend = time.time()

        return tend - tstart

    def _fresh_install_with_venv(self):
        self._remove_venv()
        try:
            os.remove(f"{self.cwd}/Pipfile.lock")
        except FileNotFoundError:
            pass
        self._create_venv()
        tstart = time.time()
        self._run(["pipenv", "install"], check=True)
        tend = time.time()

        return tend - tstart

    def _adding_package(self):
        self._ensure_lockfile()
        self._reset_pipfile()

        tstart = time.time()
        self._run(
            ["pipenv", "install", "sqlalchemy"],
            check=True,
        )
        tend = time.time()
        self._reset_pipfile()

        return tend - tstart

    def _installing_from_lockfile(self):
        self._ensure_lockfile()
        self._remove_venv()
        self._create_venv()

        tstart = time.time()
        self._install_packages()
        tend = time.time()
        return tend - tstart

    def _generating_requirements(self):
        self._ensure_lockfile()
        tstart = time.time()
        self._run(["pipenv", "lock", "-r"], check=True)
        tend = time.time()
        return tend - tstart

    def _ensure_lockfile(self):
        if not os.path.isfile(f"{self.cwd}/Pipfile.lock"):
            self._run(["pipenv", "lock"], check=True)

    def _create_venv(self):
        self._run(["pipenv", "--python", "3.8"], check=True)

    def _remove_venv(self):
        self._run(
            ["pipenv", "--rm"],
        )

    def _install_packages(self):
        self._run(["pipenv", "install"], check=True)


class Poetry(TestBase):

    DEFAULT = """
[tool.poetry]
name = "poetry"
version = "0.1.0"
description = ""
authors = ["Your Name <you@example.com>"]

[tool.poetry.dependencies]
python = "^3.8"
boto3 = "*"
pyjwt = "*"
requests = "*"
flask = "*"
cryptography = "*"

[tool.poetry.dev-dependencies]
pytest = "*"
pytest-mock = "*"
moto = "*"
pytest-randomly = "*"

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
"""

    NAME = "poetry"

    def __init__(self, n_tests):
        self.n_tests = n_tests
        self.cwd = "poetry"

    def _fresh_install_no_venv(self):
        self._remove_venv()
        try:
            os.remove(f"{self.cwd}/poetry.lock")
        except FileNotFoundError:
            pass
        tstart = time.time()
        self._run(["poetry", "install"], check=True)
        tend = time.time()

        return tend - tstart

    def _fresh_install_with_venv(self):
        self._remove_venv()
        try:
            os.remove(f"{self.cwd}/poetry.lock")
        except FileNotFoundError:
            pass
        self._create_venv()
        tstart = time.time()
        self._run(["poetry", "install"], check=True)
        tend = time.time()

        return tend - tstart

    def _adding_package(self):
        self._ensure_lockfile()
        self._reset()

        tstart = time.time()
        self._run(
            ["poetry", "add", "sqlalchemy"],
            check=True,
        )
        tend = time.time()
        self._reset()
        return tend - tstart

    def _installing_from_lockfile(self):
        self._ensure_lockfile()
        self._remove_venv()
        self._create_venv()

        tstart = time.time()
        self._install_packages()
        tend = time.time()
        return tend - tstart

    def _generating_requirements(self):
        self._ensure_lockfile()
        tstart = time.time()
        self._run(["poetry", "export", "--format", "requirements.txt"], check=True)
        tend = time.time()
        return tend - tstart

    def _install_packages(self):
        self._run(["poetry", "install"], check=True)

    def _reset(self):
        with open("poetry/pyproject.toml", "w") as outfile:
            outfile.write(self.DEFAULT)

    def _ensure_lockfile(self):
        self._run(["poetry", "lock"], check=True)

    def _create_venv(self):
        self._run(
            ["poetry", "run", "which", "python"],
            stdout=sp.PIPE,
            stderr=sp.PIPE,
            check=True,
        )

    def _remove_venv(self):
        res = self._run(
            ["poetry", "run", "which", "python"], stdout=sp.PIPE, check=True, text=True
        )
        pythonpath = res.stdout.strip()
        try:
            self._run(
                ["poetry", "env", "remove", pythonpath],
                stdout=sp.PIPE,
                stderr=sp.PIPE,
                check=True,
            )
        except sp.CalledProcessError:
            pass


def benchmark():
    n_tests = 10
    Pipenv(n_tests).benchmark()
    Poetry(n_tests).benchmark()


if __name__ == "__main__":
    benchmark()
