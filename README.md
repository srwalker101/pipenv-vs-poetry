# Comparing pipenv and poetry

## Tests:

1. Fresh install (Pipfile/pyproject.toml only)
    * no virtualenv
    * with virtualenv
2. Adding a package
3. Installing packages from lockfile
4. Generating requirements.txt

## Initial package list

* Main packages
    * `boto3`
    * `pyjwt`
    * `requests`
    * `flask`
    * `cryptography`
* Dev packages
    * `pytest`
    * `pytest-mock`
    * `moto`
    * `pytest-randomly`

Additional package: `sqlalchemy`

Both using python 3.8
