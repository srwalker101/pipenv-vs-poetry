#!/usr/bin/env python

import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=argparse.FileType("r"))
    parser.add_argument(
        "-o", "--output", required=False, default="-", type=argparse.FileType("w")
    )
    args = parser.parse_args()

    store = {}
    mode = None
    tests = set()
    for line in args.filename:
        line = line.strip()
        if "test duration" in line:
            continue

        msg = line.split("-")[-1].strip()

        if msg in {"pipenv", "poetry"}:
            store[msg] = {}
            mode = msg
        elif "complete" in msg:
            test = msg.split(":")[0].strip().replace(" complete", "")
            duration = float(msg.split(":")[-1].strip())
            store[mode][test] = duration
            tests.add(test)

    modes = store.keys()
    for test in tests:
        print(f"{test} - ", file=args.output, end="")
        for mode in modes:
            duration = store[mode][test]
            print(f"{mode}: {duration:4.2f} ", file=args.output, end="")
        poetry_speedup = store["pipenv"][test] / store["poetry"][test]
        print(f" [poetry speedup: {poetry_speedup:4.2f}x]", file=args.output)
